package me.jacob.playerrep

import me.jacob.playerrep.command.ReputationCommand
import me.jacob.playerrep.command.ReputationConfigCommand
import me.jacob.playerrep.config.SecondaryConfig
import me.jacob.playerrep.events.PlayerDamageEvent
import me.jacob.playerrep.events.PlayerDeathEvent
import me.jacob.playerrep.events.PlayerJoinEvent
import me.jacob.playerrep.utils.ReputationProcessor
import me.jacob.playerrep.utils.StaticValues
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import kotlin.concurrent.timerTask

class Plugin : JavaPlugin() {
    lateinit var databaseEngine: DatabaseEngine
    lateinit var reputationProcessor: ReputationProcessor

    override fun onEnable() {
        logger.info("Starting up...")
        saveDefaultConfig()
        databaseEngine = DatabaseEngine(
            config.getString("database.address") ?: "localhost",
            config.getInt("database.port", 3306),
            config.getString("database.name") ?: "minecraft",
            config.getString("database.user") ?: "user",
            config.getString("database.password") ?: "password",
            logger
        )
        if (config.getBoolean("logging", false)) {
            logger.handlers.forEach { logger.removeHandler(it) }
        }
        val reputationConfig = SecondaryConfig(File(dataFolder, "reputation.yml"), "reputation.yml", this)
        reputationConfig.saveDefault()
        reputationProcessor = ReputationProcessor(databaseEngine, reputationConfig.get(), logger, this)
        Bukkit.getPluginManager().registerEvents(PlayerDamageEvent(reputationProcessor), this)
        Bukkit.getPluginManager().registerEvents(PlayerDeathEvent(reputationProcessor), this)
        Bukkit.getPluginManager().registerEvents(PlayerJoinEvent(reputationProcessor), this)

        server.scheduler.scheduleSyncRepeatingTask(
            this,
            {
                timeTick()
            }, 0L, StaticValues.timerDelay * 20L
        ) //every minute
        getCommand("reputation")?.setExecutor(ReputationCommand(reputationProcessor))
        getCommand("reputationconfig")?.setExecutor(ReputationConfigCommand(reputationConfig))
        logger.info("Ready")
        super.onEnable()
    }

    override fun onDisable() {
        logger.info("Shutting down...")
        if (this.isEnabled)
            timeTick()
        super.onDisable()
    }

    fun timeTick() {
        server.scheduler.runTaskAsynchronously(this, timerTask {
            reputationProcessor.reputationSystemTick(StaticValues.timerDelay)
        })
    }
}