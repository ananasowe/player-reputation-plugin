package me.jacob.playerrep.config

import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.Plugin
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader


class SecondaryConfig(val configFile: File, val resource: String, val plugin: Plugin) {
    private var secondaryConfig = YamlConfiguration.loadConfiguration(configFile)

    fun saveDefault() {
        if (!configFile.exists()) {
            plugin.saveResource(resource, false)
        }
    }

    fun reload():Boolean {
        secondaryConfig = YamlConfiguration.loadConfiguration(configFile)

        // Look for defaults in the jar
        val defConfigStream: Reader = InputStreamReader(plugin.getResource(resource)?:return false, "UTF8")
        val defConfig = YamlConfiguration.loadConfiguration(defConfigStream)
        secondaryConfig.setDefaults(defConfig)
        return true
    }

    fun get(): FileConfiguration {
        return secondaryConfig
    }

    fun save(): Boolean {
        get().save(configFile)
        return false
    }
}