package me.jacob.playerrep.utils

import org.bukkit.ChatColor

object CommandUtil {
    operator fun ChatColor.plus(text: String) = toString() + text
    operator fun ChatColor.plus(other: ChatColor) = toString() + other.toString()
}