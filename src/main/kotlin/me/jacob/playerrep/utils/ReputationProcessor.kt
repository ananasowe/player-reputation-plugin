package me.jacob.playerrep.utils

import me.jacob.playerrep.DatabaseEngine
import net.milkbowl.vault.permission.Permission
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import org.bukkit.plugin.RegisteredServiceProvider
import java.util.*
import java.util.logging.Logger

class ReputationProcessor(val databaseEngine: DatabaseEngine, val reputationConfiguration: FileConfiguration, var logger: Logger, var plugin: Plugin) {

    fun getReputation(playerUUID: String): String {
        val playerReputation = getReputationPoints(playerUUID)
        reputationConfiguration.getKeys(false).forEach {
            val minReputation = reputationConfiguration.getInt(it.plus(".min_rep"), Int.MIN_VALUE)
            val maxReputation = reputationConfiguration.getInt(it.plus(".max_rep"), Int.MAX_VALUE)
            if (playerReputation in minReputation .. maxReputation) {
                return it
            }
        }
        return "default"
    }

    fun getPlayerColour(playerUUID: String): ChatColor {
        return ChatColor.getByChar(reputationConfiguration.getString(getReputation(playerUUID).plus(".colour")).toString()) ?: ChatColor.WHITE
    }

    fun getPlayerPrefix(playerUUID: String): String {
        return reputationConfiguration.getString(getReputation(playerUUID).plus(".name")) ?: ""
    }

    fun getReputationPoints(playerUUID: String): Int {
        return databaseEngine.getPlayer(playerUUID)?.reputation ?: 0
    }

    fun runEvent(targetPlayerUUID: String, event: String) {
        logger.info("Processing event '$event' for player of UUID $targetPlayerUUID")
        val eventPath = reputationConfiguration.getConfigurationSection(
            getReputation(targetPlayerUUID).plus(".events.").plus(event)
        )
        var minReputation = reputationConfiguration.getInt(getReputation(targetPlayerUUID).plus(".min_rep"), Int.MIN_VALUE)
        var maxReputation = reputationConfiguration.getInt(getReputation(targetPlayerUUID).plus(".max_rep"), Int.MAX_VALUE)

        if(eventPath != null) {
            databaseEngine.updatePlayer(targetPlayerUUID) {
                val initialReputation = reputation
                playerUUID = targetPlayerUUID
                if(eventPath.isInt("set_reputation"))
                    reputation = eventPath.getInt("set_reputation", 0)
                reputation += eventPath.getInt("adjust_reputation", 0)

                logger.info("Updating reputation for player with UUID $targetPlayerUUID ($initialReputation -> $reputation)")
                // Fixme: Code duplication
                if ((reputation < minReputation).or(reputation > maxReputation)) {
                    reputationConfiguration.getKeys(false).forEach {
                        minReputation = reputationConfiguration.getInt(it.plus(".min_rep"), Int.MIN_VALUE)
                        maxReputation = reputationConfiguration.getInt(it.plus(".max_rep"), Int.MAX_VALUE)
                        if (reputation in minReputation .. maxReputation) {
                            timer = reputationConfiguration.getInt(it.plus(".timer"), -1)
                            runEvent(playerUUID, "group_changed")
                            return@forEach
                        }
                    }
                }
            }
            eventPath.getStringList("run_commands").forEach {
                var command = it.replace(
                    "\$player$",
                    Bukkit.getOfflinePlayer(UUID.fromString(targetPlayerUUID)).name.toString())
                logger.info("Executing command '$command'")
                Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), command)
            }
        }
    }

    fun assignReputation(player: Player) {
        var uuid = player.uniqueId.toString()
        var reputation = getPlayerPrefix(uuid)
        val rsp: RegisteredServiceProvider<Permission>? =
            plugin.getServer().getServicesManager().getRegistration(Permission::class.java)
        var permissions = rsp?.getProvider()
        permissions?.playerRemoveGroup(player, "reputation.*")
        permissions?.playerAddGroup(player, "reputation.".plus(reputation))
        logger.info("Player of UUID $uuid has been assigned the '$reputation' reputation")
    }

    fun reputationSystemTick(timePassed: Int) {
        databaseEngine.updateTimers(timePassed) {
            this.runEvent(it.playerUUID, "time_passed")
        }
    }
}