package me.jacob.playerrep.events

import me.jacob.playerrep.utils.ReputationProcessor
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent

class PlayerDeathEvent(val reputationProcessor: ReputationProcessor) : Listener {
    @EventHandler
    fun onDeath(event: PlayerDeathEvent) {
        val player = event.entity
        val playerUUID = player.uniqueId.toString()
        if ((event.entity.killer is Player).and(player.isOnline)) {
            val killer = event.entity.killer as Player
            if(killer.isOnline) {
                val killerUUID = killer.uniqueId.toString()
                if (reputationProcessor.getReputationPoints(killerUUID) >
                    reputationProcessor.getReputationPoints(playerUUID) ||
                    killer.hasPermission("reputation.admin")
                ) {
                    reputationProcessor.runEvent(killerUUID, "privilaged_murder")
                } else {
                    reputationProcessor.runEvent(killerUUID, "murder")
                }
            }
        } else {
            if(!player.hasPermission("reputation.admin").and(player.isOnline)) {
                reputationProcessor.runEvent(playerUUID, "death")
            }
        }
    }
}