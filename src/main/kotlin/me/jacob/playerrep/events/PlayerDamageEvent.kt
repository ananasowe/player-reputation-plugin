package me.jacob.playerrep.events

import me.jacob.playerrep.utils.ReputationProcessor
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageByEntityEvent

class PlayerDamageEvent(val reputationProcessor: ReputationProcessor) : Listener {
    @EventHandler
    fun onDamage(event: EntityDamageByEntityEvent) {
        if((event.damager is Player).and(event.entity is Player)) {
            val player = event.entity as Player
            val damager = event.damager as Player
            if(player.isOnline.and(damager.isOnline).and(!damager.hasPermission("reputation.admin"))) {
                reputationProcessor.runEvent(damager.uniqueId.toString(), "damage")
            }
        }
    }
}