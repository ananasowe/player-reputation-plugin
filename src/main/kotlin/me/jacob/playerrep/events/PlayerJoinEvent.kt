package me.jacob.playerrep.events

import me.jacob.playerrep.utils.ReputationProcessor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class PlayerJoinEvent(val reputationProcessor: ReputationProcessor) : Listener {
    @EventHandler
    fun onJoin(event: PlayerJoinEvent) {
        reputationProcessor.assignReputation(event.player)
    }
}