package me.jacob.playerrep

import me.jacob.playerrep.models.Reputation
import me.jacob.playerrep.models.Reputations
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*
import java.util.logging.Logger
import kotlin.NoSuchElementException

class DatabaseEngine(val host: String, val port: Int, val databaseName: String, val username: String, val password: String, val logger: Logger) {
    val database = Database.connect(
        "jdbc:mysql://" + host + ":" + port + "/" + databaseName,
        user = username,
        password = password
    )

    init {
        transaction {
            SchemaUtils.createMissingTablesAndColumns(Reputations)
        }
    }

    fun getPlayer(targetPlayerUUID: String): Reputation? {
        var reputation: Reputation? = null
        transaction {
            reputation = Reputation.find { Reputations.playerUUID.eq(targetPlayerUUID) }.takeIf { !it.empty() }?.single() ?:
                    Reputation.new { playerUUID = targetPlayerUUID }
        }
        return reputation
    }

    fun updatePlayer(playerUUID: String, body: Reputation.() -> Unit) {
        transaction {
            val playerInstance = Reputation.find { Reputations.playerUUID eq playerUUID }.takeIf { !it.empty() }?.single()
            playerInstance?.body() ?: Reputation.new(body)
        }
    }

    fun updateTimers(amount: Int, timeoutCallback: (reputation: Reputation) -> Unit) {
        transaction {
            Reputation.all().forEach {
                if(it.timer > 0) {
                    if (it.timer - amount <= 0) {
                        it.timer = -1
                        timeoutCallback(it)
                    }
                    it.timer -= amount
                }
            }
        }
    }
}