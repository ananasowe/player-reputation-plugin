package me.jacob.playerrep.command

import me.jacob.playerrep.utils.CommandUtil.plus
import me.jacob.playerrep.utils.ReputationProcessor
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class ReputationCommand(val reputationProcessor: ReputationProcessor) : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size > 0 && sender.hasPermission("reputation.commands.check_others")) {
            val specifiedPlayer = Bukkit.getPlayer(args[0])
            if (specifiedPlayer != null) {
                val uuid = specifiedPlayer.uniqueId.toString()
                val reputationPoints = reputationProcessor.getReputationPoints(uuid)
                var colour = if (reputationPoints < 0) ChatColor.RED else ChatColor.GREEN
                sender.sendMessage(ChatColor.GRAY + "Gracz " + colour + specifiedPlayer.name + ChatColor.GRAY + " posiada " +
                        ChatColor.BOLD + colour +
                        reputationPoints
                        + ChatColor.GRAY + " punktów reputacji" + " (reputacja \"" + colour + reputationProcessor.getPlayerPrefix(uuid) + ChatColor.GRAY + "\")"
                )
            } else {
                sender.sendMessage(ChatColor.GRAY + "Nie udało się odnaleźć gracza :(")
            }
        } else {
            if (sender is Player) {
                val reputationPoints = reputationProcessor.getReputationPoints(sender.uniqueId.toString())
                sender.sendMessage(ChatColor.GRAY + "Posiadasz " +
                        ChatColor.BOLD + (if (reputationPoints < -1) ChatColor.RED else ChatColor.GREEN) +
                        reputationPoints
                        + ChatColor.GRAY + " punktów reputacji"
                )
            } else {
                sender.sendMessage("Konsola nie posiada reputacji. Podaj nazwę gracza")
            }
        }
        return true
    }
}