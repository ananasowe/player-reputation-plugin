package me.jacob.playerrep.command

import me.jacob.playerrep.config.SecondaryConfig
import me.jacob.playerrep.utils.CommandUtil.plus
import me.jacob.playerrep.utils.ReputationProcessor
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class ReputationConfigCommand(val configuration: SecondaryConfig) : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size > 0) {
            when (args[0]) {
                "reload" -> {
                    sender.sendMessage("Reloading reputation config file...")
                    if(configuration.reload()){
                        sender.sendMessage("Successfully reloaded the config.")
                    } else {
                        sender.sendMessage("There was a problem reloading the configuration. Check console for errors or restart the server.")
                    }
                }
            }
        } else {
            sender.sendMessage("Configuration loaded from: " + configuration.configFile.canonicalPath)
        }
        return true
    }


}