package me.jacob.playerrep.models

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Table

object Reputations : IntIdTable("player_reputations") {
    //val playerReputation = integer("reputation_id").uniqueIndex()
    val playerUUID = varchar("player_uuid", length = 40)
    val reputation = integer("reputation").default(0)
    val timer = integer("timer").default(-1)
}