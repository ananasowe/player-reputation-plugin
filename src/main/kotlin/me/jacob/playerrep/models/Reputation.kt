package me.jacob.playerrep.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Table

class Reputation(id: EntityID<Int>) : IntEntity(id) {
    companion object: IntEntityClass<Reputation>(Reputations)
    //var playerReputation by Reputations.playerReputation
    var playerUUID by Reputations.playerUUID
    var reputation by Reputations.reputation
    var timer by Reputations.timer
}